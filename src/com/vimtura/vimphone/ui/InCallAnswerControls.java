package com.vimtura.vimphone.ui;

import com.vimtura.vimphone.R;
import com.vimtura.vimphone.ui.SlidingTab;
import org.linphone.mediastream.Log;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class InCallAnswerControls extends RelativeLayout implements IOnLeftRightChoice {
	private static final String THIS_FILE = "InCallAnswerControls";

	private static final int MODE_LOCKER = 0;
	private static final int MODE_NO_ACTION = 1;
	
	private static final int TAKE_CALL = 1;
	private static final int DONT_TAKE_CALL = 0;
	
	private int controlMode;
	private SlidingTab slidingTabWidget;	
    private InCallAnswerControlTrigger onTriggerListener;


	public InCallAnswerControls(Context context) {
        this(context, null, 0);
    }
	
	public InCallAnswerControls(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}
	
    public InCallAnswerControls(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
        
        setGravity(Gravity.CENTER_VERTICAL);
    }
    
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		// Finalize object style
		controlMode = MODE_NO_ACTION;
	}
	
	public void setCallLockerVisibility(int visibility) {
	    controlMode = (visibility == View.VISIBLE) ? MODE_LOCKER : MODE_NO_ACTION;
	    setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            if (slidingTabWidget == null) {
                slidingTabWidget = new SlidingTab(getContext());
                slidingTabWidget.setOnLeftRightListener(this);
                this.addView(slidingTabWidget, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                slidingTabWidget.setLeftHintText(R.string.take_call);
                slidingTabWidget.setRightHintText(R.string.ignore_call);
                LayoutParams lp = (LayoutParams) slidingTabWidget.getLayoutParams();
                lp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                slidingTabWidget.setLayoutParams(lp);
                
                Resources resources = getContext().getResources();
                
        		// To sliding tab
        		slidingTabWidget.setLeftTabDrawables(resources.getDrawable(R.drawable.ic_jog_dial_answer), 
        				resources.getDrawable(R.drawable.jog_tab_target_green), 
        				resources.getDrawable(R.drawable.jog_tab_bar_left_answer), 
        				resources.getDrawable(R.drawable.jog_tab_left_answer));
        		
        		slidingTabWidget.setRightTabDrawables(resources.getDrawable(R.drawable.ic_jog_dial_decline), 
        				resources.getDrawable(R.drawable.jog_tab_target_red), 
        				resources.getDrawable(R.drawable.jog_tab_bar_right_decline), 
        				resources.getDrawable(R.drawable.jog_tab_right_decline));                
            }
        }
        
        if (slidingTabWidget != null) {
            slidingTabWidget.setVisibility(visibility);
        }
	}
	
	/**
	 * Registers a callback to be invoked when the user triggers an event.
	 * 
	 * @param listener
	 *            the OnTriggerListener to attach to this view
	 */
	public void setOnTriggerListener(InCallAnswerControlTrigger listener) {
		onTriggerListener = listener;
	}

	private void dispatchTriggerEvent(int whichHandle) {
		if (onTriggerListener != null) {
			if (whichHandle == TAKE_CALL) {
				onTriggerListener.onAnswerTrigger();
			} else if (whichHandle == DONT_TAKE_CALL) {
				onTriggerListener.onRejectTrigger();
			}
		}
	}

	@Override
	public void onLeftRightChoice(int whichHandle) {
		Log.d(THIS_FILE, "Call controls receive info from slider " + whichHandle);
		if (controlMode != MODE_LOCKER) {
			// Oups we are not in locker mode and we get a trigger from
			// locker...
			// Should not happen... but... to be sure
			return;
		}
		switch (whichHandle) {
			case LEFT_HANDLE:
				Log.d(THIS_FILE, "We take the call");
				dispatchTriggerEvent(TAKE_CALL);
				break;
			case RIGHT_HANDLE:
				Log.d(THIS_FILE, "We clear the call");
				dispatchTriggerEvent(DONT_TAKE_CALL);
			default:
				break;
		}
		
		if(slidingTabWidget != null) {
		    slidingTabWidget.resetView();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(THIS_FILE, "Hey you hit the key : " + keyCode);
        if (controlMode == MODE_LOCKER) {
    		switch (keyCode) {
    		case KeyEvent.KEYCODE_CALL:
    			dispatchTriggerEvent(TAKE_CALL);
    			return true;
    		case KeyEvent.KEYCODE_ENDCALL:
    		//case KeyEvent.KEYCODE_POWER:
    			dispatchTriggerEvent(DONT_TAKE_CALL);
    			return true;
    		default:
    			break;
    		}
        }
		
		return super.onKeyDown(keyCode, event);
	}
	

	public interface InCallAnswerControlTrigger {
		public void onAnswerTrigger();
		public void onRejectTrigger();
	}	
}
