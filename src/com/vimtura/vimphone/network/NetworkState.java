package com.vimtura.vimphone.network;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;

public class NetworkState {

	protected ConnectivityManager connectivityManager;
	protected boolean noConnectivity;
	protected NetworkInfo networkInfo;
	protected State networkState;
	protected String networkType;
	
	public NetworkState(Context context, Intent intent) {
		this((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
		noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
	}
	
	public NetworkState(ConnectivityManager connectivityManager) {
		this.connectivityManager = connectivityManager;
		this.networkInfo = connectivityManager.getActiveNetworkInfo();
		if (this.networkInfo != null) {
			this.networkState = this.networkInfo.getState();
			this.networkType = this.networkInfo.getTypeName();
		} else {
			this.noConnectivity = true;
		}
	}

	public ConnectivityManager getConnectivityManager() {
		return connectivityManager;
	}

	public boolean isNoConnectivity() {
		return noConnectivity;
	}

	public NetworkInfo getNetworkInfo() {
		return networkInfo;
	}

	public State getNetworkState() {
		return networkState;
	}

	public String getNetworkType() {
		return networkType;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof NetworkState)) {
			return false;
		}
		
		NetworkState other = (NetworkState) obj;
		
		// Compare connectivity
		//if (this.isNoConnectivity() != other.isNoConnectivity()) {
		//	return false;
		//}
		
		// Compare type
		if (this.getNetworkType() != null && other.getNetworkType() != null) {
			if (!this.getNetworkType().equals(other.getNetworkType())) {
				return false;
			}
		} else if (this.getNetworkType() != null || other.getNetworkType() != null) {
			return false;
		}
		
		// Compare state
		if (this.getNetworkState() != null && other.getNetworkState() != null) {
			if (!this.getNetworkState().equals(other.getNetworkState())) {
				return false;
			}
		} else if (this.getNetworkState() != null || other.getNetworkState() != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("NetworkState[")
		.append("networkType: ").append(networkType).append(", ")
		.append("networkState: ").append(networkState)//.append(", ")
		//.append("noConnectivity: ").append(noConnectivity)			
		.append("]");
		
		return sb.toString();
	}
	
}
