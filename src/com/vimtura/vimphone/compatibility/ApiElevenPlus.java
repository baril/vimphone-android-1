package com.vimtura.vimphone.compatibility;

import org.linphone.mediastream.Version;

import com.vimtura.vimphone.R;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;

/*
ApiElevenPlus.java
Copyright (C) 2012  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
/**
 * @author Sylvain Berfini
 */
@TargetApi(11)
public class ApiElevenPlus {
	public static void copyTextToClipboard(Context context, String msg) {
		ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE); 
	    ClipData clip = android.content.ClipData.newPlainText("Message", msg);
	    clipboard.setPrimaryClip(clip);
	}
	
	public static void setViewAlpha(Drawable view, int alpha) {
		view.setAlpha(alpha);
	}	
	
	public static void setViewAlpha(View view, float alpha) {
		view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		view.setAlpha(alpha);
	}	
}
